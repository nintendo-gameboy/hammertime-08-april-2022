# SRAM

In order to save to SRAM we must use rgbfix to set two respective flags:
* The cart type ($147) to $09: ROM + RAM + battery (see page 11, Marc Rawer, Game Boy CPU Manual)
* The RAM size ($149) to $02: 8KB (see page 12, Marc Rawer, Game Boy CPU Manual)

This allows to write to the 8KB switchable RAM bank located in the address space $A000 to $C000. Note that we are not using the bank switching properties as we specified the cartridge as being only 8KB. 

In the game logic, we have allocated two objects: 
* `sGameSaveBool`: a one byte boolean to see if the game has ever been saved before
* `sCharacterArray`: a 6400 byte allocation to store the character sprites. This space allows sequential storage of 64 bytes corresponding to each sprite (16x16 pixels) where each character needs ten sprites (standing in each direction, walking in each direction, and two additional walking sprites when the character is walking north or south; see main character Pokemon Silver sprite sheet). In this manner we can store a total of ten characters. 6400 bytes was chosen because it fits within the 8192 bytes we have in the bank switchable RAM (refered afterwards as SRAM) but still allows us to fit additional values into memory.  
