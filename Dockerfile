FROM alpine:latest AS INSTALL

RUN apk add --update \
      build-base \
      bison \
      libpng-dev \
      git


RUN git clone https://github.com/gbdev/rgbds.git
WORKDIR /rgbds
RUN make Q='' all

FROM alpine:latest as MAKE
RUN apk add --update \
      libpng \
      make \
      gcc \
      python3-dev \
      linux-headers \
      libc-dev \
      py3-pip

RUN pip3 install bitarray

COPY --from=INSTALL \
  /rgbds/rgbasm \
  /rgbds/rgbfix \
  /rgbds/rgblink \
  /rgbds/rgbgfx \
  /bin/
COPY / /gdbev/