ASM             :=  rgbasm
LINKER          :=  rgblink
FIX             :=  rgbfix

OUTPUT		:=  main.gb
SYMFILE		:=  main.sym
INC_DIR		:=  src/constants/
INC_DIR2	:=  src/text/
INC_DIR3	:=  src/code/
INC_DIR4	:=  src/gfx/
MAIN_DIR	:=  src/code/
ASM_FLAGS       :=  -i $(INC_DIR) -i $(INC_DIR2) -i $(INC_DIR3) -i $(INC_DIR4) -o

.PHONY: all clean

all: fix

fix: build
	$(FIX) -v -p 0 -m 8 -r 2 $(OUTPUT)

build: asm
	@echo "[+] Linking"
	$(LINKER) -d -t -o $(OUTPUT) main.o -n $(SYMFILE)

asm:
	@echo "[+] Assembling"
	python3 makePacket.py
	mv packet.bin src/gfx/.
	$(ASM) $(ASM_FLAGS) main.o $(MAIN_DIR)main.asm
