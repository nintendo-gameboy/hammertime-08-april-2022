from bitarray import bitarray
from math import ceil

char_to_tiles = {
    'A':   "DB      %..XX.... \n \
            DB      %.XXXX... \n \
            DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %XXXXXX.. \n \
            DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %........",
 
    'B' :  "DB      %XXXXXX.. \n \
            DB      %.XX..XX. \n \
            DB      %.XX..XX. \n \
            DB      %.XXXXX.. \n \
            DB      %.XX..XX. \n \
            DB      %.XX..XX. \n \
            DB      %XXXXXX.. \n \
            DB      %........",
 
    'C':   "DB      %..XXXX.. \n \
            DB      %.XX..XX. \n \
            DB      %XX...... \n \
            DB      %XX...... \n \
            DB      %XX...... \n \
            DB      %.XX..XX. \n \
            DB      %..XXXX.. \n \
            DB      %........",
 
    'D' :  "DB      %XXXXX... \n \
            DB      %.XX.XX.. \n \
            DB      %.XX..XX. \n \
            DB      %.XX..XX. \n \
            DB      %.XX..XX. \n \
            DB      %.XX.XX.. \n \
            DB      %XXXXX... \n \
            DB      %........",
 
    'E' :  "DB      %.XXXXXX.n \
            DB      %.XX.....\n \
            DB      %.XX.....\n \
            DB      %.XXXX...\n \
            DB      %.XX.....\n \
            DB      %.XX.....\n \
            DB      %.XXXXXX.\n \
            DB      %........",

    'F' :  "DB      %.XXXXXX. \n \
            DB      %.XX..... \n \
            DB      %.XX..... \n \
            DB      %.XXXX... \n \
            DB      %.XX..... \n \
            DB      %.XX..... \n \
            DB      %.XX..... \n \
            DB      %........",

    'G' :  "DB      %..XXXX.. \n \
            DB      %.XX..XX. \n \
            DB      %XX...... \n \
            DB      %XX...... \n \
            DB      %XX..XXX. \n \
            DB      %.XX..XX. \n \
            DB      %..XXXXX. \n \
            DB      %........",
    'H' :  "DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %XXXXXX.. \n \
            DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %........",

    'I' :  "DB      %.XXXX...\n \
            DB      %..XX....\n \
            DB      %..XX....\n \
            DB      %..XX....\n \
            DB      %..XX....\n \
            DB      %..XX....\n \
            DB      %.XXXX...\n \
            DB      %........",

    'J' :  "DB      %...XXXX. \n \
            DB      %....XX.. \n \
            DB      %....XX.. \n \
            DB      %....XX.. \n \
            DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %.XXXX... \n \
            DB      %........",

    'K' :  "DB      %XXX..XX. \n \
            DB      %.XX..XX. \n \
            DB      %.XX.XX.. \n \
            DB      %.XXXX... \n \
            DB      %.XX.XX.. \n \
            DB      %.XX..XX. \n \
            DB      %XXX..XX. \n \
            DB      %........",

    'L' :  "DB      %.XX..... \n \
            DB      %.XX..... \n \
            DB      %.XX..... \n \
            DB      %.XX..... \n \
            DB      %.XX..... \n \
            DB      %.XX..... \n \
            DB      %.XXXXXX. \n \
            DB      %........",

    'M':   "DB      %XX...XX. \n \
            DB      %XXX.XXX. \n \
            DB      %XXXXXXX. \n \
            DB      %XXXXXXX. \n \
            DB      %XX.X.XX. \n \
            DB      %XX...XX. \n \
            DB      %XX...XX. \n \
            DB      %........",

    'N':   "DB      %XX...XX. \n \
            DB      %XXX..XX. \n \
            DB      %XXXX.XX. \n \
            DB      %XX.XXXX. \n \
            DB      %XX..XXX. \n \
            DB      %XX...XX. \n \
            DB      %XX...XX. \n \
            DB      %........",

    'O' :  "DB      %..XXX... \n \
            DB      %.XX.XX.. \n \
            DB      %XX...XX. \n \
            DB      %XX...XX. \n \
            DB      %XX...XX. \n \
            DB      %.XX.XX.. \n \
            DB      %..XXX... \n \
            DB      %........",

    'P' :  "DB      %XXXXXX.. \n \
            DB      %.XX..XX. \n \
            DB      %.XX..XX. \n \
            DB      %.XXXXX.. \n \
            DB      %.XX..... \n \
            DB      %.XX..... \n \
            DB      %XXXX.... \n \
            DB      %........",

    'Q' :  "DB      %.XXXX... \n \
            DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %XX.XXX.. \n \
            DB      %.XXXX... \n \
            DB      %...XXX.. \n \
            DB      %........",

    'R' :  "DB      %XXXXXX.. \n \
            DB      %.XX..XX. \n \
            DB      %.XX..XX. \n \
            DB      %.XXXXX.. \n \
            DB      %.XX.XX.. \n \
            DB      %.XX..XX. \n \
            DB      %XXX..XX. \n \
            DB      %........",

    'S' :  "DB      %.XXXX... \n \
            DB      %XX..XX.. \n \
            DB      %XXX..... \n \
            DB      %.XXXX... \n \
            DB      %...XXX.. \n \
            DB      %XX..XX.. \n \
            DB      %.XXXX... \n \
            DB      %........",

    'T' :  "DB      %XXXXXX.. \n \
            DB      %..XX.... \n \
            DB      %..XX.... \n \
            DB      %..XX.... \n \
            DB      %..XX.... \n \
            DB      %..XX.... \n \
            DB      %..XX.... \n \
            DB      %........",

    'U':   "DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %XXXXXX.. \n \
            DB      %........",

    'V' :  "DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %.XXXX... \n \
            DB      %..XX.... \n \
            DB      %........ ",

    'W' :  "DB      %XX...XX. \n \
            DB      %XX...XX. \n \
            DB      %XX...XX. \n \
            DB      %XX.X.XX. \n \
            DB      %XXXXXXX. \n \
            DB      %XXX.XXX. \n \
            DB      %XX...XX. \n \
            DB      %........",

    'X' :  "DB      %XX...XX. \n \
            DB      %XX...XX. \n \
            DB      %.XX.XX.. \n \
            DB      %..XXX... \n \
            DB      %..XXX... \n \
            DB      %.XX.XX.. \n \
            DB      %XX...XX. \n \
            DB      %........",
    'Y' :  "DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %XX..XX.. \n \
            DB      %.XXXX... \n \
            DB      %..XX.... \n \
            DB      %..XX.... \n \
            DB      %.XXXX... \n \
            DB      %........",

    'Z' :  "DB      %XXXXXXX. \n \
            DB      %.....XX. \n \
            DB      %....XX.. \n \
            DB      %...XX... \n \
            DB      %..XX.... \n \
            DB      %.XX..... \n \
            DB      %XXXXXXX. \n \
            DB      %........",

    ' ':   "DB      %........\n \
            DB      %........\n \
            DB      %........\n \
            DB      %........\n \
            DB      %........\n \
            DB      %........\n \
            DB      %........\n \
            DB      %........"
}

MAX_DATA_SIZE = 0x280

def make_bits(s):
    """ 
    Function takes a string, maps it to the array above
    and produces a bytearray of tiles where each tile 
    represents two bits that map back to a palette.
    @parameter s: a string (ie, "TEST"); the characters must be present in the array above.
    @return tiles: a bytearray of tiles
    """
    tiles = bytearray()
    
    for char in s:                                      # for each character we see in the string
        for line in char_to_tiles[char].splitlines():      # get each line of tileset (each line is a byte)
            stripped_line = ''.join(i for i in line if i in [".", "X"])
            replaced_line = stripped_line.replace('.', '0')
            replaced_line = replaced_line.replace('X', '1')

            bits = bitarray(replaced_line, endian='big')
            tiles += bits.tobytes()
            
    return tiles

def calc_checksum(tiles):
    """
    Take a bytearray of tiles that exist in the header and data segments.
    Adds up the bytes to return a two byte number in little endian that is the
    checksum of the packet.
    @parameter tiles: a bytearray of tiles
    @return: a bytearray consisting of two bytes
    """
    sum = 0
    for byte in tiles:
        sum += byte
    return sum.to_bytes(2, byteorder='little')

def make_data_packet(tiles):
    """
    Takes a bytearray of tiles, chunks into 0x260 bytes (maximum size)
    and properly formats the packet. 
    @parameter tiles: a bytearray of tiles
    @return a list of bytearrays where each element is a packet.
    """
    
    preamble    =   bytearray(b"\x88\x33")
    packet_type =   bytearray(b"\x04")
    compression =   bytearray(b"\x00")
    size        =   bytearray(b"\x80\x02")      # hard coded, must be this size
    
    if len(tiles) > MAX_DATA_SIZE: 
        print("too long")
        return
        #TODO
        

    if len(tiles) < MAX_DATA_SIZE:                      # we need to pad
        pad_size = MAX_DATA_SIZE - len(tiles)
        pad_byte = b"\x00"

        padding = bytearray(pad_size * pad_byte)
        data = tiles + padding

    else:
        data = tiles
        
    checksum = calc_checksum(packet_type+compression+size+data)
    dummy = bytearray(b"\x00\x00")
    
    packet = preamble 
    packet += packet_type
    packet += compression
    packet += size
    packet += data
    packet += checksum
    packet += dummy

    return packet


if __name__ == "__main__":
    # convert ascii to tile bits (00/01/10/11)
    # the reason there exists spaces is because the printer writes one horizontally,
    # and then moved down vertically before going back up. In effect, the spaces
    # cause the second line to be blank.
    first_strip = make_bits("I   H E L P E D   O U T   T H E   C S D   A N D   A L L   I   G O T   W A S")
    second_strip = make_bits("            T H I S   L A M E                      C E R T I F I C A T E")
    

    with open('packet.bin', 'ab') as w:
        # an array of bytearrays where each element represents MAX_DATA_SIZE data segments for a data packet
        for element in (first_strip, second_strip):
            packet = make_data_packet(element)    # turn those tiles into a data packet
            w.write(packet)


