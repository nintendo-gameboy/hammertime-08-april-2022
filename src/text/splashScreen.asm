ascii_tiles:
	chr_IBMPC1	1, 4	; spit out 256 ascii characters here in rom
				; (params 1, 8 specify we want all 256 chars)

; ending label (ascii_tiles_end) gives us a memory address that we can use in
; mem_Copy* routine. Since we need both starting and ending memory addresses
; in order to calculate number of bytes to copy (stored in register-pair BC)
ascii_tiles_end:

splash_tiles0:
	DB "0"
end_splash_tiles0:

splash_tiles1:
	DB "1"
end_splash_tiles1:

go_army_beat_navy:
	DB "10001110110111100100000010000010111001001101101011110010010000001000010011001010110000101110100001000000100111001100001011101100111100100"
go_army_beat_navy_end:

csd_title:
DB $3F,$3F,$E0,$E0,$E0,$E0,$E0,$E0
DB $81,$81,$63,$63,$01,$01,$00,$60
DB $60,$3F,$3F,$00,$00,$00,$00,$00
DB $60,$83,$83,$00,$00,$00,$00,$00
DB $FC,$FC,$00,$00,$F0,$F0,$0C,$0C
DB $0C,$F0,$F0,$00,$00,$00,$00,$00
DB $7F,$7F,$70,$70,$70,$70,$70,$70
DB $70,$7F,$7F,$00,$00,$00,$00,$00
DB $C0,$C0,$30,$30,$30,$30,$30,$30
DB $30,$C0,$C0,$00,$00,$00,$00,$00
csd_title_end:


SetTitleSprites:

	ld	a, %11111111
	ld	[rOBP0],	a

	PutSpriteXAddr	x0y0, 60 
	PutSpriteYAddr	x0y0, 60
	sprite_PutTile	x0y0, $80 ;TODO get rid of hard code
	sprite_PutFlags	x0y0, $00

	PutSpriteXAddr	x1y0, 68 
	PutSpriteYAddr	x1y0, 60
	sprite_PutTile	x1y0, $81 ;TODO get rid of hard code
	sprite_PutFlags	x1y0, $00

	PutSpriteXAddr	x2y0, 76
	PutSpriteYAddr	x2y0, 60
	sprite_PutTile	x2y0, $84 ;TODO get rid of hard code
	sprite_PutFlags	x2y0, $00


	PutSpriteXAddr	x3y0, 84
	PutSpriteYAddr	x3y0, 60
	sprite_PutTile	x3y0, $86 ;TODO get rid of hard code
	sprite_PutFlags	x3y0, $00


	PutSpriteXAddr	x4y0, 92
	PutSpriteYAddr	x4y0, 60
	sprite_PutTile	x4y0, $88 ;TODO get rid of hard code
	sprite_PutFlags	x4y0, $00
	

	PutSpriteXAddr	x0y1, 60
	PutSpriteYAddr	x0y1, 68
	sprite_PutTile	x0y1, $82 ;TODO get rid of hard code
	sprite_PutFlags	x0y1, $00

	PutSpriteXAddr	x1y1, 68
	PutSpriteYAddr	x1y1, 68
	sprite_PutTile	x1y1, $83 ; TODO get rid of hard code
	sprite_PutFlags	x1y1, $00

	PutSpriteXAddr	x2y1, 76
	PutSpriteYAddr	x2y1, 68
	sprite_PutTile	x2y1, $85 ; TODO get rid of hard code
	sprite_PutFlags	x2y1, $00

	
	PutSpriteXAddr	x3y1, 84
	PutSpriteYAddr	x3y1, 68
	sprite_PutTile	x3y1, $87 ; TODO get rid of hard code
	sprite_PutFlags	x3y1, $00

	PutSpriteXAddr	x4y1, 92
	PutSpriteYAddr	x4y1, 68
	sprite_PutTile	x4y1, $89 ; TODO get rid of hard code
	sprite_PutFlags	x4y1, $00


SECOND_LINE_X	EQU	54	

	PutSpriteXAddr	arrow, SECOND_LINE_X
	PutSpriteYAddr	arrow, 90
	sprite_PutTile	arrow, $10 ; TODO get rid of hard code
	sprite_PutFlags	arrow, $00


	PutSpriteXAddr	s, SECOND_LINE_X + 16 	; give a little space
	PutSpriteYAddr	s, 90
	sprite_PutTile	s, "s" ; TODO get rid of hard code
	sprite_PutFlags	s, $00


	PutSpriteXAddr	t, SECOND_LINE_X + 21
	PutSpriteYAddr	t, 90
	sprite_PutTile	t, "t" ; TODO get rid of hard code
	sprite_PutFlags	t, $00
	
	PutSpriteXAddr	a, SECOND_LINE_X + 26
	PutSpriteYAddr	a, 90
	sprite_PutTile	a, "a" ; TODO get rid of hard code
	sprite_PutFlags	a, $00
	
	PutSpriteXAddr	r, SECOND_LINE_X + 34
	PutSpriteYAddr	r, 90
	sprite_PutTile	r, "r" ; TODO get rid of hard code
	sprite_PutFlags	r, $00

	
	PutSpriteXAddr	t1, SECOND_LINE_X + 42
	PutSpriteYAddr	t1, 90
	sprite_PutTile	t1, "t" ; TODO get rid of hard code
	sprite_PutFlags	t1, $00

	ret
