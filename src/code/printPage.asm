PRINTER_KEEP_ALIVE	EQU	$81
MAX_DATA_SIZE		EQU	$280
PRINT_STATUS_READY	EQU	$08

include "noPrinterConnectedPage.asm"

SECTION "Printer", ROM0
LoadPrint:
	call 	lcd_Stop

	call	ClearBG
	call	ClearOAM

	ld	bc, thank_you_cert_end-thank_you_cert	; size
	ld	de, thank_you_cert			; source
	ld	hl, _SCRN0				; dest, background
	call	Memcpy

	; turn on LCD

	ld	a, [rLCDC]	; fetch LCD Config. (Each bit is a flag)
	or	LCDCF_ON
	ld	[rLCDC], a	; save LCD Config. Sprites are now visible.

.loop
	halt
	nop

	; check if debouncer ready
	ld	a, [wDebouncer]
	cp	0
	jr	nz, .loop

	ld	a, DEBOUNCER_MAGIC_NUM
	ld	[wDebouncer], a

	; check if the a button hit
	call 	jpad_GetKeys
	ld	d, a			; save results

.checkA
	and	PADF_A
	jr	z, .checkB
	ld	hl, thank_you_cert	; source
	call 	PrintCert		; print!
	jr	.loop			; return to loop

.checkB
	ld	a, d			; retrieve result of GetKeys
	and 	PADF_B
	jr	z, .loop
	ret				; we want to return to the next page

	jr	.loop

PrintCert:

	call ClearPrinterHandShakeFlags			; zero out our printer status/keep-alive

	ld	de, NUL_packet				; NUL should respond with a keep-alive of $81
	ld	bc, NUL_packet_end - NUL_packet
	call	SendToPrinter
	call 	CheckPrinterHandShake			; make sure printer is there.
	jr	nz, .handleStatusError

	call 	ClearPrinterHandShakeFlags

	; send printer init
	ld	de, init_printer			; source
	ld	bc, init_printer_end - init_printer	; size
	call	SendToPrinter

	call 	ClearPrinterHandShakeFlags

	; send first packet
	ld	de, data_packet			 	; source
	ld	bc, data_packet_end - data_packet	; size, data_packet consists of TWO packets
	call	SendToPrinter

	call 	ClearPrinterHandShakeFlags


	; send data end
	ld	de, data_end_packet			 	; source
	ld	bc, data_end_packet_end - data_end_packet	; size
	call	SendToPrinter	; returns $08

	call ClearPrinterHandShakeFlags

	; send print
	ld	de, print_packet			 	; source
	ld	bc, print_packet_end - print_packet	; size
	call	SendToPrinter


	; TODO should really check on the status of the printer.
.return
	ret

.handleStatusError
	ld	a, [wPrinterHandShake]
	ld	b, a
	ld	a, [wPrinterStatusFlags]
	ld	c, a
	push	bc
	call 	LoadNoPrinterConnectedPage
	ret

.clearBG
	call ClearBG
	ret

; Takes a source and writes it to serial out [rSB]
; @param $bc: size to send
; @param $de: source to move into rSB
; @return:    void
SendToPrinter:
        ld 	a, [de] 	; grab one byte from the source
        ldh 	[rSB], a 	; place it at dest
	ld	a, %10000001	; initiate transfer
	ldh	[rSC], a	; store in serial control

	; wait for transfer to complete
.loop
	halt
	ld	a, [rSC]	; load rSC back into a
	and	%10000000	; check if highest bit is ON, if so, transfer has not happened yet
	cp	%10000000
	jr	z, .loop

	;halt 			; wait for a serial interrupt. this is not stable.

        inc de 			; move to next byte
        dec bc 			; decrement count
        ld a, b 		; check if count is zero
        or c
        jr nz, SendToPrinter	; send next byte
        ret

CheckPrinterHandShake:
	ld	a, [wPrinterHandShake]
	cp	PRINTER_KEEP_ALIVE
	ret

CheckPrinterStatusFlags:
	ld	a, [wPrinterStatusFlags]
	ret

ClearPrinterHandShakeFlags:
	xor	a
	ld	[wPrinterHandShake], a
	ld	[wPrinterStatusFlags], a
	ret

; Loads the printer handshake and status flags.
; Displays them in the top left corner of the screen.
; The handshake should be 1 if the handshake occured, otherwise 0.
; @return: void
DebugShowPrinterStatus:
	call 	lcd_Stop

	ld	a, [wPrinterHandShake]
	cp	$81
	jr	nz, .loadZero
	ld	a, $31
	ld	[_SCRN0], a
	jr	.statusFlags
.loadZero
	ld	a, $30
	ld	[_SCRN0], a
.statusFlags
	ld	a, [wPrinterStatusFlags]
	add	a, $30
	ld	[_SCRN0 + 2], a

	ld	a, [rLCDC]	; fetch LCD Config. (Each bit is a flag)
	or	LCDCF_ON
	ld	[rLCDC], a	; save LCD Config. Sprites are now visible.
	ret


; TODO these need to find a new home.
b_button:
	incbin "b_button.bin", 0, 16 * 9 	; include 9 tiles at 16 bytes a tile
b_button_end:


thank_you_cert:
	DB "                                "
	DB "I helped out the CSD"
	DB "             "
	DB "and all I got was "
	DB "                   "
	DB "this lame"
	DB "                      "
	DB "certificate"
	DB "                "
	DB "                               "
	DB "                               "
	DB "                               "
	DB "                               "
	DB "                               "
	DB "             "
	DB "A: Print                        "
	DB "B: Back"
thank_you_cert_end:


; this packet must be sent before sending data packets to the printer.
; this will reset the printer's internal RAM.
; normal status: $81 and $00
; not connected: $FF and $FF
init_printer:
	DB $88, $33, $01, $00, $00, $00, $01, $00, $00, $00
init_printer_end:

; a functionless packet for requesting the current status of the printer.
; the printer may occasionally jam, etc, so this should be sent first.
; normal status: $81 and $00
; not connected: $FF and $FF
NUL_packet:
	DB $88, $33, $0F, $00, $00, $00, $0F, $00, $00, $00
NUL_packet_end:

; a data length of 0 for the data packet header represents the end of the print data.
; this must always be sent to end print data transmission.
; normal status: ?
data_end_packet:
	DB $88, $33, $04, $00, $00, $00, $04, $00, $00, $00
data_end_packet_end:

; used to discontinue printing. the break packet is sent by means of the user's instructions
; and forcibly stops printing.
; normal status: ?
break_packet:
	DB $88, $33, $08, $00, $00, $00, $08, $00, $00, $00
break_packet_end:

print_packet:
	DB 	$88, $33	; preamble
	; header segment follows
	DB	$02 		; packet type ($02 = print)
	DB 	$00		; compression ($00 = off)
	DB	$04, $00	; size of data segment (LSB first)
	; data segment follows
	DB	$01		; number of sheets
	DB 	$48		; number of line feeds
	DB 	$E4		; palette values
	DB	$40		; print density
	; checksum segment follows
	DB	$73, $01	; checksum (LSB first)
	; dummy segment follows
	DB	 $00, $00
print_packet_end:

; normal status: $81, $00
data_packet:
	INCBIN "packet.bin"
data_packet_end:
