
LoadCredits:

	call 	lcd_Stop
	
	call	ClearBG
	call	ClearOAM
	
	ld	bc, credits_end - credits	; size
	ld	de, credits			; source
	ld	hl, _SCRN0 + $24		; destination, centers on second line
	call	Memcpy

	; turn on LCD

	ld	a, [rLCDC]	; fetch LCD Config. (Each bit is a flag)
	or	LCDCF_ON
	ld	[rLCDC], a	; save LCD Config. Sprites are now visible. 
	
.loop
	halt
	nop
	
	; check if debouncer ready
	ld	a, [wDebouncer]
	cp	0
	jr	nz, .loop

	ld	a, DEBOUNCER_MAGIC_NUM	; reset debouncer
	ld	[wDebouncer], a

	; check if the a button hit
	call 	jpad_GetKeys
	ld	d, a			; save results
	
.checkB
	ld	a, d			; retrieve result of GetKeys		
	and 	PADF_B
	jr	z, .loop
.return
	
	ret				; we want to return to the next page

credits:
	DB "Sean Deaton"
credits_end:
