NO_PRINTER_CONN_ERR	EQU	$FF
CHECKSUM_ERR_STAT	EQU	$01
CURRENTLY_PRINTING_STAT	EQU	$02
DATA_FULL_STAT		EQU	$04
UNPROCESSED_DATA_STAT	EQU	$08
PACKET_ERR_STAT		EQU	$10
PAPER_JAM_STAT		EQU	$20
OTHER_ERR_STAT		EQU	$40
LOW_BATTERY_STAT	EQU	$80


LoadNoPrinterConnectedPage:

	call 	lcd_Stop
	
	call	ClearBG
	call	ClearOAM

	pop 	bc
	ld	a, b	; b contains wPrinterHandShake, c constains wPrinterStatusFlags

.noPrinterError
	cp	PRINTER_KEEP_ALIVE	
	jr	z, .checksumError

	ld	bc, no_printer_message_end - no_printer_message	; size
	ld	de, no_printer_message				; source
	ld	hl, _SCRN0					; dest, background
	jr	.memcpy

.checksumError
	ld	a, c
	and	CHECKSUM_ERR_STAT					; compare bit 0 with 1, set NZ if present
	jr	z, .currentlyPrinting
	ld	bc, checksum_error_message_end - checksum_error_message	; size
	ld	de, checksum_error_message					; source
	ld	hl, _SCRN0						; dest, background
	jr	.memcpy

.currentlyPrinting
	ld	a, c
	and	CURRENTLY_PRINTING_STAT					; compare bit 0 with 1, set NZ if present
	jr	z, .dataFull	
	ld	bc, printing_message_end - printing_message		; size
	ld	de, printing_message					; source
	ld	hl, _SCRN0						; dest, background
	jr	.memcpy

.dataFull
	ld	a, c
	and	CURRENTLY_PRINTING_STAT					; compare bit 0 with 1, set NZ if present
	jr	z, .unprocessedData	
	ld	bc, data_full_message_end - data_full_message		; size
	ld	de, data_full_message					; source
	ld	hl, _SCRN0						; dest, background
	jr	.memcpy

.unprocessedData
	ld	a, c
	and	UNPROCESSED_DATA_STAT						; compare bit 0 with 1, set NZ if present
	jr	z, .return	
	ld	bc, unprocessed_data_message_end - unprocessed_data_message	; size
	ld	de, unprocessed_data_message					; source
	ld	hl, _SCRN0							; dest, background
	jr	.memcpy

.memcpy	
	call	Memcpy

	; turn on LCD

	ld	a, [rLCDC]	; fetch LCD Config. (Each bit is a flag)
	or	LCDCF_ON
	ld	[rLCDC], a	; save LCD Config. Sprites are now visible. 
	
.loop
	halt
	nop
	
	; check if debouncer ready
	ld	a, [wDebouncer]
	cp	0
	jr	nz, .loop

	ld	a, DEBOUNCER_MAGIC_NUM
	ld	[wDebouncer], a

	; check if the a button hit
	call 	jpad_GetKeys
	ld	d, a		; save results
	
.checkB
	ld	a, d		; retrieve result of GetKeys		
	and 	PADF_B
	jr	z, .loop
.return
	ret			; we want to return to the next page


no_printer_message:
	DB "                                "
	DB "No printer connected!           "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "        B: Back                 "
no_printer_message_end:

checksum_error_message:
	DB "                                "
	DB "Packet checksum                 "
	DB "error.                          "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "        B: Back                 "
checksum_error_message_end:
printing_message:
	DB "                                "
	DB "Currently printing.             "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "        B: Back                 "
printing_message_end:
data_full_message:
	DB "                                "
	DB "Data full.                      "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "        B: Back                 "
data_full_message_end:
unprocessed_data_message:
	DB "                                "
	DB "Currently printing.             "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "                                "
	DB "        B: Back                 "
unprocessed_data_message_end:
