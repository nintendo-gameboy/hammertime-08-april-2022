PLAYER_SELECT_CENTER	EQU	$23
OBJ_VRAM		EQU	$8800
MAX_CHARACTERS		EQU	20
CHARACTER_SIZE		EQU	64
SPRITES_PER_CHARACTER	EQU	10

CHARACTER_POS_X EQU	69
CHARACTER_POS_Y EQU	35

SECTION "CharacterSelect", ROM0
LoadCharacterSelect:
	call 	lcd_Stop
	call	ClearBG
	call	ClearOAM

	; display character select string
	ld	bc, characterSelectStringEnd - characterSelectString 	; size
	ld	de, characterSelectString				; source
	ld	hl, _SCRN0 + PLAYER_SELECT_CENTER			; dest
	call 	Memcpy

	; declare the sprite for this page
	SpriteAttr	character_half_1
	SpriteAttr	character_half_2

	; define pallete
	ld      a, %00011011
        ld      [rOBP0], a

	; load starter character into VRAM
	ld	bc, CHARACTER_SIZE
	ld	de, sCharacterArray
	ld	hl, OBJ_VRAM
	call	Memcpy

	; load arrow to VRAM
	ld	bc, CHARACTER_SIZE / 2
	ld	de, arrow_sprite
	ld	hl, OBJ_VRAM + (CHARACTER_SIZE)
	call	Memcpy
	
	; load starter from VRAM to screen
	PutSpriteXAddr	character_half_1, CHARACTER_POS_X
	PutSpriteYAddr	character_half_1, CHARACTER_POS_Y
	sprite_PutTile	character_half_1, $80
	sprite_PutFlags character_half_1, $00


	PutSpriteXAddr	character_half_2, CHARACTER_POS_X + 8
	PutSpriteYAddr	character_half_2, CHARACTER_POS_Y
	; add two because it will be loaded two tiles away since we enabled 16-bit mode
	sprite_PutTile	character_half_2, $80 + 2
	sprite_PutFlags character_half_2, $00

	PutSpriteXAddr	arrow, CHARACTER_POS_X - 16
	PutSpriteYAddr	arrow, CHARACTER_POS_Y + 4
	sprite_PutTile	arrow, $80 + 4
	sprite_PutFlags	arrow, $00 

	; turn on LCD
        ld      a, [rLCDC]      ; fetch LCD Config. (Each bit is a flag)
	or      LCDCF_OBJ16	; enable 16-bit long sprites
        or      LCDCF_ON
        ld      [rLCDC], a      ; save LCD Config. Sprites are now visible. 

	xor	b			; save our current index into the character array
	push	bc
	ld	hl, sCharacterArray	; save memory address
	push	hl

.loop

	halt
	nop

	; check if debouncer ready
	ld	a, [wDebouncer]
	cp	0
	jr	nz, .loop

	ld	a, DEBOUNCER_MAGIC_NUM
	ld	[wDebouncer], a

.getKeys		
	; check to see what key was pressed
	call	jpad_GetKeys
	ld	d, a			; save result of call.

.moveRight
	and	PADF_RIGHT
	jp	z, .moveLeft		; right NOT depressed

	pop	hl
	; increment index into character array
	pop	bc
	inc	b
	; update memory address
	ld	de, (SPRITES_PER_CHARACTER * CHARACTER_SIZE)
	add	hl, de
	
	; check array bounds (max 20)
	ld	a, b
	cp	a, 20
	jr	nz, .updateCharacterRight	; not twenty
	; a == 20, reset to 0
	ld	b, 0
	ld	hl, sCharacterArray

.updateCharacterRight
	
	; save values
	push 	bc
	push	hl

	; load character into VRAM
	ld	bc, 64
	pop	de
	push 	de
	ld	hl, OBJ_VRAM
	call	Memcpy
	
	
	;jr 	.loop		

.moveLeft
	; TODO could optimize this and not call it again
	call	jpad_GetKeys
	and 	PADF_LEFT
	jp	z, .loop		; left NOT depressed
	
	; decrement array and memory address
	pop	hl
	pop 	bc

	dec	b
	; there is no 16-bit SUB, so we add and overflow
	ld	de, $FFFF-(SPRITES_PER_CHARACTER * CHARACTER_SIZE)+1
	add	hl, de

	ld	a, b
	cp	a, 255				; test if we're at the begining before updating
	jp	nz, .updateCharacterLeft	; not 255
	; a == 0, change to max characters
	ld	b, MAX_CHARACTERS - 1
	ld	hl, sCharacterArray + (CHARACTER_SIZE * SPRITES_PER_CHARACTER) * (MAX_CHARACTERS - 1)

.updateCharacterLeft
	push	bc
	push 	hl

	; load character into VRAM
	ld	bc, CHARACTER_SIZE
	pop	de
	push 	de
	ld	hl, OBJ_VRAM
	call	Memcpy


	jr .loop


arrow_sprite:
	incbin "arrow.bin"
arrow_sprite_end:

characterSelectString:
	DB "Player Select"
characterSelectStringEnd:

petullo:
	incbin "petullo.bin"
petulloEnd:

missingNo:
	incbin "missingno.bin"
missingNoEnd:
