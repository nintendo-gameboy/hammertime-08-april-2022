; allow user to select character or print certificate.
; input: void
; output: void
LoadSecondPage:

CHAR_BG_TILE	EQU	$9862
PRINT_BG_TILE	EQU	$98E8
TRADE_BG_TILE	EQU	$9965
CREDITS_BG_TILE	EQU	$99E6
ARROW_ASCII 	EQU	$10

ARROW_POS0_X	EQU	5	; X for "Select Character"
ARROW_POS0_Y	EQU	25	; Y for "Select Character"

ARROW_POS1_X	EQU	53	; X for "Print"
ARROW_POS1_Y	EQU	55	; Y for "Print"

ARROW_POS2_X	EQU	28	; X for "Link Trade"
ARROW_POS2_Y	EQU	88	; Y for "Link Trade"

ARROW_POS3_X	EQU	35	; X for "Credits"
ARROW_POS3_Y	EQU	120	; Y for "Credits"

	call 	lcd_Stop
	call	ClearOAM	; clear all sprites.
	call	ClearBG		; clear background TODO consolidate

	; reset window
	xor	a
	ld	[rSCX], a

	; display character select on the background
	ld	hl, CHAR_BG_TILE			; destination:
	ld	de, char_select_string			; source: char select
	ld	bc, char_select_string_end - char_select_string	; count
	call Memcpy

	; display print on the background
	ld	hl, PRINT_BG_TILE			; destination:
	ld	de, print_string			; source: print
	ld	bc, print_string_end - print_string	; count
	call Memcpy

	; display link trade on the background
	ld	hl, TRADE_BG_TILE			; destination:
	ld	de, trade_string			; source: print
	ld	bc, trade_string_end - trade_string	; count
	call Memcpy


	; display credits on the background
	ld	hl, CREDITS_BG_TILE			; destination:
	ld	de, credits_string			; source: print
	ld	bc, credits_string_end - credits_string	; count
	call Memcpy

	; load the arrow as a sprite.

	PutSpriteXAddr	arrow, 5
	PutSpriteYAddr	arrow, 25
	sprite_PutTile	arrow, ARROW_ASCII
	sprite_PutFlags	arrow, $00

	; turn LCD back on
	ld	a, [rLCDC]	; fetch LCD Config. (Each bit is a flag)
	or	LCDCF_ON
	ld	[rLCDC], a	; save LCD Config. Sprites are now visible.

	ld	e, $00		; set arrow state (0-3)
.loop
	halt	; halts cpu until interrupt triggers (vblank)
 	; by halting, we ensure that .loop only runs only each screen-refresh,
	; so only 60fps. That makes the sprite movement here manageable
	nop

	; check if debouncer ready
	ld	a, [wDebouncer]
	cp	0
	jr	nz, .loop

	ld	a, DEBOUNCER_MAGIC_NUM
	ld	[wDebouncer], a

.getKeys
	; check to see what key was pressed
	call	jpad_GetKeys
	ld	d, a			; save result of call.

.moveNextPage
	ld	a, d
	and	PADF_A
	jp	z, .moveArrowUp
	jp	.checkSelection

.moveArrowUp
	; move arrow up
	ld 	a, d
	and	PADF_UP			; set NZ if present
	jr	z, .moveArrowDown	; if not present, move on
	dec	e			; increase arrow state
	jp	.setArrowPosition
.moveArrowDown
	ld	a, d			; check result again
	and	PADF_DOWN
	jp	z, .loop
	inc	e



.setArrowPosition
	ld	a, e			; move arrow state into $a
	and	%00000011		; mask it to limit to 0-3
	ld	e, a			; load back into $e
.checkOne
	cp 	%00000001		; check if one
	jr	nz, .checkTwo
	; set position
	ld	b, ARROW_POS1_X
	ld	c, ARROW_POS1_Y

.checkTwo
	cp 	%00000010		; check if one
	jr	nz, .checkThree
	; set position
	ld	b, ARROW_POS2_X
	ld	c, ARROW_POS2_Y

.checkThree
	cp 	%00000011		; check if one
	jr	nz, .checkZero
	; set position
	ld	b, ARROW_POS3_X
	ld	c, ARROW_POS3_Y

.checkZero
	cp	%00000000
	jr	nz, .setPosition
	ld	b, ARROW_POS0_X
	ld	c, ARROW_POS0_Y
.setPosition
	PutSpriteXAddr	arrow, 	b
	PutSpriteYAddr	arrow, 	c
	jp .loop


; check where the arrow is located.
; jump table.
.checkSelection
	GetSpriteYAddr	arrow		; $a <- where the arrow located
	cp	ARROW_POS0_Y
	jr	z, .loadCharacterSelect
	cp	ARROW_POS1_Y
	jr	z, .loadPrint
	cp	ARROW_POS2_Y
	jr	z, .loadLinkTrade
	cp	ARROW_POS3_Y
	jr	z, .loadCredits
	; uh oh
	jr .loop

.loadCharacterSelect
	call LoadCharacterSelect
	ret
.loadPrint
	call LoadPrint
	ret
.loadLinkTrade

.loadCredits
	call LoadCredits
	ret


char_select_string:
	DB "Select Character"
char_select_string_end:

trade_string:
	DB "Link Trade"
trade_string_end:

print_string:
	DB "Print"
print_string_end:

credits_string:
	DB "Credits"
credits_string_end:
