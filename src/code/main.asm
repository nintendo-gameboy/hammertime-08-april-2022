include "gbhw.inc"
include "dma.inc"
include "sprite.inc"

DEBOUNCER_MAGIC_NUM	EQU	$7


;-------------- INTERRUPT VECTORS ------------------------
; specific memory addresses are called when a hardware interrupt triggers

SECTION "Vblank", ROM0[$0040]
	JP	VBLANK_INT
	;reti

SECTION "LCDC", ROM0[$0048]
	reti

SECTION "Timer", ROM0[$0050]
	;JP	TIMER_INT
	reti

SECTION "Serial", ROM0[$0058]
	JP	SERIAL_INT
	;reti

SECTION "Joypad", ROM0[$0060]
	reti
;----------- END INTERRUPT VECTORS -------------------

SECTION "ROM_entry_point", ROM0[$0100]	; ROM is given control from boot here
	nop
	jp	code_begins

;------------- BEGIN ROM HEADER ----------------
; The gameboy reads this info (before handing control over to ROM)
SECTION "rom header", ROM0[$0104]
	NINTENDO_LOGO
	ROM_HEADER	"0123456789ABCDE"

; by convention, *.asm files add code to the ROM when included. *.inc files
; do not add code. They only define constants or macros. The macros add code
; to the ROM when called

include	"ibmpc1.inc"	; used to generate ascii characters in our ROM
include "memory.asm"	; used to copy Monochrome ascii characters to VRAM

; declare CSD sprites

	SpriteAttr	x0y0
	SpriteAttr	x1y0
	SpriteAttr	x2y0
	SpriteAttr	x3y0
	SpriteAttr	x4y0

	SpriteAttr	x0y1
	SpriteAttr	x1y1
	SpriteAttr	x2y1
	SpriteAttr	x3y1
	SpriteAttr	x4y1

	; declare select sprites
	SpriteAttr	arrow
	SpriteAttr	s
	SpriteAttr	t
	SpriteAttr	a
	SpriteAttr	r
	SpriteAttr	t1	; t is already defined, appending a 1
		
	
code_begins:
	di	; disable interrupts
	ld	SP, $FFFF	; set stack to top of HRAM
	
	dma_Copy2HRAM

	ld	a, IEF_VBLANK	; enable VBLANK intterupts
	or	IEF_SERIAL
	ld	[rIE], a

	xor	a
	ld 	[wDebouncer], a	; set debouncer
	ld	[wCheatSeq], a	; set cheatSeq
	ei

	call	lcd_Stop
	; load ascii tiles (inserted below with chr_IBMPC1 macro) into VRAM
	ld	hl, ascii_tiles	; ROM address where we insert ascii tiles
	ld	de, _VRAM	; destination. Going to copy ascii to video ram
	; bc = byte-count. Aka how many bytes to copy
	ld	bc, ascii_tiles_end - ascii_tiles
	call	mem_CopyMono	; copies monochrome tiles, specifically
				; (our ascii set is monochrome)
	; load the title tiles
	ld 	hl, csd_title
	ld	de, $8800	; tried using the length of ascii_tiles but didn't work. hard coding :(
	ld	bc, csd_title_end - csd_title
	call	mem_CopyMono

	; set up saved game if not already present
	ld	a, [sBoolSaveGamePresent]
	cp	$0
	jr	z, .setSplashScreenVars
	
	; if no save game present (boolSaveGamePresent == 0)
	; load the start character into the character array
	ld	bc, petulloEnd - petullo
	ld	de, petullo
	ld	hl, sCharacterArray
	call	Memcpy

	; TODO remove this in production
	; copy in test characters
	ld	bc, missingNoEnd - missingNo
	ld	de, missingNo
	ld	hl, sCharacterArray + (64 * 10)
	call	Memcpy
	

	; set save game check
	ld	a, $1
	ld 	[sBoolSaveGamePresent], a

.setSplashScreenVars
	ld	hl, _SCRN0	; destination 0x9800
	ld	bc, (_SCRN1 - _SCRN0) / (go_army_beat_navy_end - go_army_beat_navy)	; total times to copy

.load
	ld	de, go_army_beat_navy	; source
	push 	bc	; save times to copy
	ld	bc, go_army_beat_navy_end - go_army_beat_navy	; load size of string
	call Memcpy

	pop	bc	; retrieve total size to copy
	dec 	bc
	ld	a, b
	or	c
	jr	nz, .load

.LCDOn
	;ld	a, IEF_VBLANK	; --
	;ld	[rIE], a	; Set only Vblank interrupt flag
	ei			; enable interrupts. Only vblank will trigger

	call ClearOAM	

	ld	a, [rLCDC]	; fetch LCD Config. (Each bit is a flag)
	or	LCDCF_OBJON	; enable sprites through "OBJects ON" flag
	or	LCDCF_OBJ8	; enable 8bit wide sprites (vs. 16-bit wide)
	or	LCDCF_ON
	ld	[rLCDC], a	; save LCD Config. Sprites are now visible. 

	; change background of scrolling 0/1's
	ld	a, %00000001
	ld	[rBGP], a

	call SetTitleSprites
	
.loop
	halt	; halts cpu until interrupt triggers (vblank)
 	; by halting, we ensure that .loop only runs only each screen-refresh,
	; so only 60fps. That makes the sprite movement here manageable
	nop
	ld	a,b
	ld	[rSCX],a
	inc	a
	ld	b,a
	
	; check if debouncer ready
	ld	a, [wDebouncer]
	cp	0
	jr	nz, .loop
	
	; reset debouncer
	ld	a, DEBOUNCER_MAGIC_NUM
	ld	[wDebouncer], a

	; check which key pressed
	call 	jpad_GetKeys
	push	af				; save register $A (joypad info)
	and	PADF_START			; compare to START button. set NZ flag is present	
	
	jp	nz, .secondPageLoop		; move to next page

.checkLeft
	pop	af
	push 	af
	and	PADF_LEFT
	jp	z, .checkRight
	ld	a, [wCheatSeq]			; add one to cheatSeq
	add	a, %00000001
	ld	[wCheatSeq], a		

.checkRight
	pop	af
	push	af
	and	PADF_RIGHT
	jr	z, .checkUp
	ld	a, [wCheatSeq]			; add two to cheatSeq
	add	a, %00000010
	ld	[wCheatSeq], a		
	
.checkUp
	pop	af
	push	af
	and	PADF_RIGHT
	jr	z, .checkDown
	ld	a, [wCheatSeq]			; add two to cheatSeq
	add	a, %00000100
	ld	[wCheatSeq], a		

.checkDown
	pop	af
	and	PADF_RIGHT
	jr	z, .checkCheatSeq
	ld	a, [wCheatSeq]			; add two to cheatSeq
	add	a, %00001000
	ld	[wCheatSeq], a		

.checkCheatSeq
	ld	a, [wCheatSeq]
	cp	%00011110
	jr	z, .loop			; TODO

	jr	.loop


.loadCheatScreen
	call	ClearBG
	jr	.loop


.secondPageLoop
	call 	LoadSecondPage
	jr	.secondPageLoop			; start up at top of .loop label. Repeats each vblank

; You can turn off LCD at any time, but it's bad for LCD if NOT done at vblank
lcd_Stop:
	ld	a, [rLCDC]	; LCD-Config
	and	LCDCF_ON	; compare config to lcd-on flag
	ret	z		; return if LCD is already off
.wait4vblank
	ldh	a, [rLY]   ; ldh is a faster version of ld if in [$FFxx] range
	cp	145  ; are we at line 145 yet?  (finished drawing screen then)
	jr	nz, .wait4vblank
.stopLCD
	ld	a, [rLCDC]
	xor	LCDCF_ON	; XOR lcd-on bit with lcd control bits. (toggles LCD off)
	ld	[rLCDC], a	; `a` holds result of XOR operation
	ret

include "splashScreen.asm"
include "secondPage.asm"
include "printPage.asm"
include "credits.asm"
include "characterSelect.asm"

Memcpy:
        ld a, [de] ; grab one byte from the source
        ld [hli], a ; place it at dest, increment hl
        inc de ; move to next byte
        dec bc ; decrement count
        ld a, b ; check if count is zero
        or c
        jr nz, Memcpy
        ret
ZeroBytes:
        ld a, [de] ; grab one byte from the source
        ld [hli], a ; place it at dest, increment hl
        ;inc de ; move to next byte
        dec bc ; decrement count
        ld a, b ; check if count is zero
        or c
        jr nz, ZeroBytes
        ret

jpad_GetKeys:
; Uses AF, B
; get currently pressed keys. Register A will hold keys in the following
; order: MSB --> LSB (Most Significant Bit --> Least Significant Bit)
; Down, Up, Left, Right, Start, Select, B, A
; This works by writing
	push 	bc	; save for scrolling
	; get action buttons: A, B, Start / Select
	ld	a, JOYPAD_BUTTONS; choose bit that'll give us action button info
	ld	[rJOYPAD], a; write to joypad, telling it we'd like button info
	ld	a, [rJOYPAD]; gameboy will write (back in address) joypad info
	ld	a, [rJOYPAD]
	cpl		; take compliment
	and	$0f	; look at first 4 bits only  (lower nibble)
	swap	a	; place lower nibble into upper nibble
	ld	b, a	; store keys in b
	; get directional keys
	ld	a, JOYPAD_ARROWS
	ld	[rJOYPAD], a ; write to joypad, selecting direction keys
	ld	a, [rJOYPAD]
	ld	a, [rJOYPAD]
	ld	a, [rJOYPAD]	; delay to reliablly read keys
	ld	a, [rJOYPAD]	; since we've just swapped from reading
	ld	a, [rJOYPAD]	; buttons to arrow keys
	ld	a, [rJOYPAD]
	cpl			; take compliment
	and	$0f		; keep lower nibble
	or	b		; combine action & direction keys (result in a)
	ld	b, a

	ld	a, JOYPAD_BUTTONS | JOYPAD_ARROWS
	ld	[rJOYPAD], a		; reset joypad

	ld	a,b	; register A holds result. Each bit represents a keyi
	pop 	bc
	ret

; TODO these need a new home

VBLANK_INT:
	call	DMA_ROUTINE
	
	ld	a, [wDebouncer]
	cp	0		; check if debouncer is done 
	jr	z, .ret
	dec 	a
	ld	[wDebouncer], a
.ret
	reti

TIMER_INT:
	;ld	a, $00
	;ld	[$C000], a
	reti

SERIAL_INT:
	ld	a, [rSB]	; grab the serial byte
	; we found the handshake
	cp	$81		; keep-alive
	jp	nz, .checkStatus
	ld	a, $81
	ld	[wPrinterHandShake], a
	jr	.return

.checkStatus
	ld	a, [wPrinterHandShake]		; check to see if the handshake has already happened.
	cp	$81
	jr	nz, .return 			; handshake hasn't been received, this is not the status flag
	ld	a, [rSB]			; otherwise we have the status code
	ld	[wPrinterStatusFlags], a 
	jr	.return 	
	
.return
	reti

SECTION "test", WRAM0
wPrinterStatusFlags:: db
wPrinterHandShake:: db

wPadding:: ds 70
wDebouncer:: db
wCheatSeq:: db


; banked external (save) RAM section
SECTION "CharactersUnlocked", SRAM

; is save game present
sBoolSaveGamePresent:: ds 1 

; allocate space for 10 characters, each sprite being 64 bytes (16x16), ten directions
sCharacterArray:: ds (64 * 10) * 10 
